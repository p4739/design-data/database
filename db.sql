drop database if exists agricultureportal;

create database agricultureportal;

use agricultureportal;

-- Id	FN	LN	Email	Password	Contact No	Role_id	image_id (Foreign Key)	Pincode_id(Foreign key)
-- foreign keys remaining
drop table if exists user;
create table role(roleId int primary key,name varchar(10));

-- PINCODE id	Pincode	State	District	City
create table pincode(pincodeId int primary key,state varchar(15),district varchar(15),city varchar(10));

-- IMAGE Id	Name	Image/Video (link)
create table image(imageId int primary key auto_increment,name varchar(50),link varchar(500));


create table user(userId int primary key auto_increment, firstName varchar(30),
lastName varchar(30), email varchar(30), password  varchar(1000), contactNo varchar(30),roleId int,pincodeId int,imageId int,
foreign key(roleId) REFERENCES role(roleId),
foreign key(pincodeId) REFERENCES pincode(pincodeId),foreign key(imageId) REFERENCES image(imageId));


-- ADDRESS id	Address Line 1	Address Line 2	User_id (foreign key)	Landmark
-- FOREIGN KEY (PersonID) REFERENCES Persons(PersonID)
create table address(addId int primary key auto_increment,addressLine1 varchar(50),addressLine2 varchar(50),
userId int, landmark varchar(30), foreign key(userId) REFERENCES user(userId));


-- Measurement	 id	unit
create table measurement(measurementId int primary key, unit varchar(15));

-- Category			id	Name	image_id(Foreign Key)	isMachinery
create table category(categoryId int primary key auto_increment,name varchar(30),imageId int,isMachinery int, 
                        foreign key(imageId) REFERENCES image(imageId));

-- SUB CATEGORY
create table sub_category (subCategoryId int primary key auto_increment,name varchar(30), categoryId int, imageId varchar(100),
utilImageId varchar(100), description text,measurementId int, price varchar(100), hasVariety int,
foreign key(categoryId) REFERENCES category(categoryId),foreign key(measurementId) REFERENCES measurement(measurementId));

-- Variety																															
create table variety(varietyId int primary key auto_increment,name varchar(30),subCategoryId int, price float,
imageId int,description text,
foreign key(imageId) REFERENCES image(imageId),foreign key(subCategoryId) REFERENCES sub_category(subCategoryId));

-- Product				
create table product(productId int primary key auto_increment,subCategoryId int, varietyId int, quantity int, userId int,
foreign key(subCategoryId) REFERENCES sub_category(subCategoryId),foreign key(varietyId) REFERENCES variety(varietyId),
foreign key(userId) REFERENCES user(userId));

-- Question
create table question(queId int primary key auto_increment,question varchar(100),answer text,
expertId int, querierId int,createdTimestamp timestamp default CURRENT_TIMESTAMP,foreign key(expertId) REFERENCES user(userId),
foreign key(querierId) REFERENCES user(userId));

-- Comment																															
create table comment(commentId int primary key auto_increment,questionId int, userId int, 
createdTimestamp timestamp default CURRENT_TIMESTAMP,
comment text,foreign key(questionId) REFERENCES question(queId),foreign key(userId) REFERENCES user(userId));

-- CART
create table cart(cartId int primary key auto_increment,productId int, quantity int, userId int,
foreign key(userId) REFERENCES user(userId),foreign key(productId) REFERENCES product(productId));

-- Order Status	id	Status_name
create table order_status(orderStatusId int primary key, statusName varchar(40));

-- ORDER
create table orders(orderId int primary key auto_increment,userId int, totalAmount float, paymentMethod varchar(40) default 'COD',
createdTimestamp timestamp default CURRENT_TIMESTAMP,statusId int, foreign key(userId) REFERENCES user(userId),
foreign key(statusId) REFERENCES order_status(orderStatusId));

-- ORDER DETAILS
create table order_details(orderDetailsId int primary key auto_increment,orderId int, productId int, quantity int, 
productTotalAmount float,
foreign key(orderId) REFERENCES orders(orderId),foreign key(productId) REFERENCES product(productId));

-- Sub topic agri 
create table sub_topic_agri(subTopicAgriId int primary key auto_increment,subCategoryId int, imageId varchar(500),
 seasons varchar(500),irrigation varchar(500),diseases varchar(500),weedManagement varchar(500),fertiliezersAndPesticides varchar(500),
foreign key(subCategoryId) REFERENCES sub_category(subCategoryId));

-- Sub topic machine
create table sub_topic_machine(subTopicMcId int primary key auto_increment,varietyId int,subCategoryId int, imageId varchar(200),
functions varchar(100),specType varchar(100),	specPower varchar(100),specDimension varchar(100),specWeight varchar(60),
specCapacity varchar(100),feature varchar(400),info text,
foreign key(varietyId) REFERENCES variety(varietyId));


-- view query
Create view vw_user AS Select user.userId,user.firstName,user.lastName,user.email,user.password,
user.contactNo,user.roleId,user.imageId,user.pincodeId,address.addId,address.addressLine1,address.addressLine2,
address.landmark,pincode.state,pincode.district,pincode.city from User 
left join Address on User.userId = Address.userId 
left join Pincode on User.PincodeId = Pincode.PincodeId;


-- order,order status,order details, product, sub_category,

-- Select orders.orderId, orders.userId, orders.totalAmount, orders.createdTimestamp,
-- orders.statusId,orders.paymentMethod,order_status.statusName,order_details.orderDetailsId,
-- order_details.quantity,order_details.totalAmount,product.subCategoryId,product.varietyId,
-- product.quantity,sub_category.name,sub_category.categoryId,sub_category.measurementId,sub_category.price
--  from orders left join on order.statusId = order_status.orderStatusId
--  left join order_details on order_details.orderDetailsId = orders.orderId
--  left join product on product.productId = order_details.productId
--  left join sub_category on product.subCategoryId = sub_category.subCategoryId;


-- products page query 
-- select p.productId,sub.name,p.quantity,sub.price, p.userId, sub.hasVariety,sub.subCategoryId from product p
-- inner join sub_category sub on p.subCategoryId = sub.subCategoryId where p.userId=9;

-- view of category sub variet and product
create view vw_products as  select c.name as catName,sub.measurementId,p.productId,sub.name as subName,
p.quantity,sub.price AS subPrice, p.userId, sub.hasVariety,sub.subCategoryId,
v.name as vName,v.price as vPrice,v.varietyId
from product p
left join sub_category sub on p.subCategoryId = sub.subCategoryId
join category c on  sub.categoryId = c.categoryId
left join variety v on v.subCategoryId = sub.subCategoryId;


-- orders an order status view
-- select o.*,os.statusName from orders o inner join order_status os on o.statusId = os.orderStatusId;

-- select order_details.totalAmount,order_details.productId,orders.createdTimestamp,order_details.quantity
-- from order_details join orders on order_details.orderId = orders.orderId
-- where order_details.orderId = 1;

-- order details view page
create view vw_orderdetails as 
select order_details.orderId,orders.totalAmount,orders.createdTimestamp,orders.statusId,
order_details.productId,order_details.quantity,order_details.productTotalAmount as productsAmount,
product.subCategoryId,product.varietyId,product.quantity as productQty,
sub_category.name,sub_category.price as subCatPrice,sub_category.hasVariety,
variety.name as varietyName, variety.price as varietyPrice
from orders left join order_details 
on orders.orderId = order_details.orderId
left join product on order_details.productId = product.productId
left join sub_category on product.subCategoryId = sub_category.subCategoryId
left join variety on product.varietyId = variety.varietyId;



-- -------------------------STATIC DATA--------------------------------------------

-- role data
insert into role values(1,'customer');
insert into role values(2,'farmer');
insert into role values(3,'expert');

-- pincode data
insert into pincode values(411028,'maharashtra','pune','pune');
insert into pincode values(411002,'maharashtra','pune','pune');
insert into pincode values(411001,'maharashtra','pune','pune');

-- measurement data
insert into measurement values(1,'kg');
insert into measurement values(2,'litre');
insert into measurement values(3,'unit');

-- order status details
insert into order_status values(1,'processing');
insert into order_status values(2,'Out for delivery');
insert into order_status values(3,'delivered');

-- ------------------------------------------------------------------------------------

insert into image(name,link) values("Cereal","https://vedangprojectimages.s3.amazonaws.com/Cereals.jpg");
insert into image(name,link) values("Livestock","https://vedangprojectimages.s3.amazonaws.com/livestock.jpg");
insert into image(name,link) values("Fruits","https://vedangprojectimages.s3.amazonaws.com/Fruits.jpg");
insert into image(name,link) values("Pulses","https://vedangprojectimages.s3.amazonaws.com/Pulses.jpg");
insert into image(name,link) values("Vegetables","https://vedangprojectimages.s3.amazonaws.com/Vegetable.jpg");
insert into image(name,link) values("Millets","https://vedangprojectimages.s3.amazonaws.com/Millets.jpg");
insert into image(name,link) values("Seeds","https://vedangprojectimages.s3.amazonaws.com/seeds.jpg");
insert into image(name,link) values("Sapling","https://vedangprojectimages.s3.amazonaws.com/sapling.jpg");
insert into image(name,link) values("Farm Machinery","https://vedangprojectimages.s3.amazonaws.com/FarmMachinery.jpg");
insert into image(name,link) values("Tractor1","https://vedangprojectimages.s3.amazonaws.com/Tractor1.png");
insert into image(name,link) values("Tractor2","https://vedangprojectimages.s3.amazonaws.com/Tractor2.webp");
insert into image(name,link) values("Machine equipment1","https://vedangprojectimages.s3.amazonaws.com/machinery_equipment.jpg");
insert into image(name,link) values("Machine equipment2","https://vedangprojectimages.s3.amazonaws.com/machinery_equipment2.jpg");
insert into image(name,link) values("Machine equipment3","https://vedangprojectimages.s3.amazonaws.com/machine_equipment3.webp");
INSERT INTO image (`imageId`, `name`, `link`) VALUES ('15', 'Egg', 'https://vedangprojectimages.s3.amazonaws.com/egg.jpg');
INSERT INTO image (`imageId`, `name`, `link`) VALUES ('16', 'Onion', 'https://vedangprojectimages.s3.amazonaws.com/onion.jpg');
INSERT INTO image (`imageId`, `name`, `link`) VALUES ('17', 'Vegetable', 'https://vedangprojectimages.s3.amazonaws.com/vegetables.jpg');
insert into image(name,link) values("tomato","https://vedangprojectimages.s3.amazonaws.com/Tomato.jpg");
insert into image(name,link) values("apple","https://vedangprojectimages.s3.amazonaws.com/Apple.jfif");
insert into image(name,link) values("rice","https://vedangprojectimages.s3.amazonaws.com/Rice.jpg");
insert into image(name,link) values("kiwi","https://vedangprojectimages.s3.amazonaws.com/kiwi.jfif");
insert into image(name,link) values("mango","https://vedangprojectimages.s3.amazonaws.com/Mango.jfif");
insert into image(name,link) values("pineapple","https://vedangprojectimages.s3.amazonaws.com/Pineapple.jfif");
insert into image(name,link) values("dal","https://vedangprojectimages.s3.amazonaws.com/dal.jfif");
insert into image(name,link) values("wheat","https://vedangprojectimages.s3.amazonaws.com/wheat.jpg");
insert into image(name,link) values("milk","https://vedangprojectimages.s3.amazonaws.com/milk.jpg");
insert into image(name,link) values("methi","https://vedangprojectimages.s3.amazonaws.com/methi.jfif");
insert into image(name,link) values("spinach","https://vedangprojectimages.s3.amazonaws.com/spinach.jpg");
insert into image(name,link) values("jawar","https://vedangprojectimages.s3.amazonaws.com/jowar.jpg");

insert into category(name,imageId,isMachinery) values('Cereal',1,0);
insert into category(name,imageId,isMachinery) values('Livestock',2,0);
insert into category(name,imageId,isMachinery) values('Fruits',3,0);
insert into category(name,imageId,isMachinery) values('Pulses',4,0);
insert into category(name,imageId,isMachinery) values('Vegetables',5,0);
insert into category(name,imageId,isMachinery) values('Millets',6,0);
insert into category(name,imageId,isMachinery) values('Seeds',7,0);
insert into category(name,imageId,isMachinery) values('Crop Saplings',8,0);
insert into category(name,imageId,isMachinery) values('Farm Machinery',9,1);

insert into sub_category(name,categoryId,imageId,measurementId,price,hasVariety)
values('eggs',2,'15#16#17',3,100,1);
insert into sub_category(name,categoryId,imageId,measurementId,price,hasVariety)
values('tomato',5,'18',3,100,0);
insert into sub_category(name,categoryId,imageId,measurementId,price,hasVariety)
values('apple',3,'19',3,100,1);
insert into sub_category(name,categoryId,imageId,measurementId,price,hasVariety) 
values('rice',1,'20',1,1200,1);
insert into sub_category(name,categoryId,imageId,measurementId,price,hasVariety)
values('kiwi',3,'21',3,100,0);
insert into sub_category(name,categoryId,imageId,measurementId,price,hasVariety)
values('mango',3,'22',3,100,1);
insert into sub_category(name,categoryId,imageId,measurementId,price,hasVariety)
values('pineapple',3,'23',3,100,0);
insert into sub_category(name,categoryId,imageId,measurementId,price,hasVariety)
values('dal',4,'24',1,120,1);
insert into sub_category(name,categoryId,imageId,measurementId,price,hasVariety)
values('wheat',1,'25',1,1200,1);
insert into sub_category(name,categoryId,imageId,measurementId,price,hasVariety)
values('milk',2,'26',2,100,0);
insert into sub_category(name,categoryId,imageId,measurementId,price,hasVariety)
values('methi',5,'27',3,100,1);
insert into sub_category(name,categoryId,imageId,measurementId,price,hasVariety)
values('spinach',5,'28',3,100,0);
insert into sub_category(name,categoryId,imageId,measurementId,price,hasVariety)
values('Jawar',6,'29',1,120,1);
insert into sub_category(name,categoryId,imageId,measurementId,price,hasVariety)
values('Tractor',9,'10#11',3,120,1);
insert into sub_category(name,categoryId,imageId,measurementId,price,hasVariety)
values('Attchements',9,'12#13#14',3,120,1);

-- UPDATE sub_category SET `imageId` = '15#16#17' WHERE (`subCategoryId` = '1');
-- UPDATE sub_category SET `imageId` = '12#13#14' WHERE (`subCategoryId` = '15');
-- UPDATE sub_category SET `imageId` = '10#11' WHERE (`subCategoryId` = '14');

insert into variety(name,subCategoryId,price) values('Basmati',4,300);
insert into variety(name,subCategoryId,price) values('kashmiri',4,100);
insert into variety(name,subCategoryId,price) values('moong',8,50);
insert into variety(name,subCategoryId,price) values('alphenso',6,150);
insert into variety(name,subCategoryId,price) values('Biryani',4,300);
insert into variety(name,subCategoryId,price) values('Indrayani',4,100);
insert into variety(name,subCategoryId,price) values('Toor',8,50);
insert into variety(name,subCategoryId,price) values('Devgad',6,150);
insert into variety(name,subCategoryId,price) values('Trolley',15,150);
insert into variety(name,subCategoryId,price) values('Rotor',15,150);
insert into variety(name,subCategoryId,price) values('Seeder',15,150);
insert into variety(name,subCategoryId,price) values('Cultivator',15,150);


-- user sample data
insert into user (firstName, lastName, email, password, roleId, pincodeId) values ('Nachiket','Jagdale','nj@test.com', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', 2, 411028 );
INSERT INTO user (`userId`, `firstName`, `lastName`, `email`, `password`, `roleId`, `pincodeId`) VALUES ('2', 'Tanmay', 'Mandaogane', 'tm@test.com', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', '3', '411001');
INSERT INTO user (`userId`, `firstName`, `lastName`, `email`, `password`, `roleId`, `pincodeId`) VALUES ('3', 'Vedang', 'Parab', 'vp@test.com', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', '2', '411002');
INSERT INTO user (`userId`, `firstName`, `lastName`, `email`, `password`, `roleId`, `pincodeId`) VALUES ('4', 'Mayur', 'Kapurkar', 'mk@test.com', 'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db', '1', '411028');

INSERT INTO address (addressLine1,addressLine2,userId,landmark) values ("behind govt hospital","budhwar bazar road",2,"near SOS");
INSERT INTO address (addressLine1,addressLine2,userId,landmark) values ("B 403 prathamesh garden","hadapsar",1,"near amdocs");
INSERT INTO address (addressLine1,addressLine2,userId,landmark) values ("house no 226 deourwada","borim ponda goa",3,"near navdurga temple");
INSERT INTO address (addressLine1,addressLine2,userId,landmark) values ("rajnandan residency azad colony","khagashiv nagar karad",4,"emerson company");



-- product sample data
insert into product(subCategoryId,varietyId,quantity,userId)
values(2,null,12,1);
insert into product(subCategoryId,varietyId,quantity,userId)
values(4,1,10,1);
insert into product(subCategoryId,varietyId,quantity,userId)
values(4,2,10,1);

insert into product(subCategoryId,varietyId,quantity,userId)
values(15,9,4,1);
insert into product(subCategoryId,varietyId,quantity,userId)
values(15,12,10,1);

insert into product(subCategoryId,varietyId,quantity,userId)
values(4,2,10,3);
insert into product(subCategoryId,varietyId,quantity,userId)
values(5,null,10,3);

-- orders sample data
insert into orders(userId,totalAmount,statusId) values(1,2600,1); -- rice [basmati] qty = 2kg (600rs)+ apple [kashmiri] 20 units(2000)
insert into orders(userId,totalAmount,statusId) values(1,2500,2); -- kiwi 10 units (1000rs) mango[alphenso] 10 units (1500)
insert into orders(userId,totalAmount,statusId) values(1,1500,3); -- pineapple qty = 9units (900rs) dal[moond]  qty = 12kg (600rs)


insert into sub_topic_agri(subCategoryId,imageId,seasons,irrigation,diseases,weedManagement,
fertiliezersAndPesticides) values(1,"1#2#3",'Dry season with assured irrigation is more suitable',
'Irrigation only to moist the soil in the early period of 10 days.
Restoring irrigation to a maximum depth of 2.5cm after development of hairline cracks in the soil 
until panicle initiation.Increasing irrigation depth to 5.0cm after PI one day after disappearance of
ponded water','Sheath spot','Using rotary weeder / Cono weeder / power operated two row weeder',
'Lambda-cyhalothrin, malathion and zeta-cypermethrin');

insert into sub_topic_agri(subCategoryId,imageId,seasons,irrigation,diseases,weedManagement,
fertiliezersAndPesticides) 
values(2,"4#5#6",'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature
from 45 BC, making it over 2000 years old. Richard McClintock, 
a Latin professor at Hampden-Sydney College in Virginia,
looked up one of the more obscure Latin words,','Lorem Ipsum',
' when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
It has survived not only five centuries','Lambda-cyhalothrin, malathion and zeta-cypermethrin');

insert into sub_topic_machine(varietyId,imageId,functions,specType,specPower,
specDimension,specWeight,specCapacity,feature,info) 
values(10,"10#11#12",'All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary',
' when an unknown printer took a galley of type and scrambled it to make a type specimen book',
'45 HP','125 inchs','200','Using rotary weeder / Cono weeder / power operated two row weeder',
'Compact tractors, as their name implies, are small',
'high-powered tractors that can assist with all the basic functions needed on a farm.');

insert into sub_topic_machine(varietyId,imageId,functions,specType,specPower,specDimension,specWeight,specCapacity,feature,info) 
values(9,"14#13#12",'All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary',
' when an unknown printer took a galley of type and scrambled it to make a type specimen book',
'45 HP',
'125 inchs',
'200',
'Using rotary weeder / Cono weeder / power operated two row weeder',
'Compact tractors, as their name implies, are small, high-powered tractors that can assist with all the basic functions needed on a farm.',
'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.');


insert into order_details(orderId,productId,quantity,productTotalAmount) values(1,2,2,600);
insert into order_details(orderId,productId,quantity,productTotalAmount) values(1,3,20,2000);

insert into order_details(orderId,productId,quantity,productTotalAmount) values(2,4,10,1000);
insert into order_details(orderId,productId,quantity,productTotalAmount) values(2,5,10,1500);

insert into order_details(orderId,productId,quantity,productTotalAmount) values(3,6,9,900);
insert into order_details(orderId,productId,quantity,productTotalAmount) values(3,7,12,600);


insert into cart (productId,quantity,userId) values(1,3,4);
insert into cart (productId,quantity,userId) values(1,2,2);
insert into cart (productId,quantity,userId) values(3,2,4);
insert into cart (productId,quantity,userId) values(7,1,2);
insert into cart (productId,quantity,userId) values(3,2,3);

insert into question (question,answer,expertId,querierId) values ( "What are the factors affecting cereal cultivation?","The field establishment of grain crops is the percentage of the sown seed that goes on to produce established plants. At sowing, management and environment are the key issues.Factors affecting the establishment percentage include management factors such as depth of sowing, row spacing, seed size and herbicide application as well as environmental factors such as soil moisture and temperature. The presence of pests and diseases also affect crop establishment.",3,4);
insert into question (question,answer,expertId,querierId) values ("Why dairy animals especially buffaloes do not show estrus in summer season?","The main reason is due to heat stress and lack of green fodder availability. Farmers are advised to reduce stress by keeping animals in shady areas especially under trees, providing fans/coolers and clean drinking water facilities in the shed. Provision of green fodder (Hay/silage can be used as an alternative in summer months) and allowing the animals for wallowing in ponds especially to buffaloes are also helpful to reduce the effect of summer stress.",3,2);
insert into question (question,answer,expertId,querierId) values ("How to improve and maintain of Fruit Germplasm?","Fruit crops have been cultivated for centuries, both commercially and in amateur gardens, and many species covering a wide range of trees, shrubs, and nonwoody perennials have been the long-term subjects of germplasm improvement programs. For example, different varieties of apples (Malus spp.) were recorded by the ancient Greeks, and selection and breeding of apple have continued since then. In contrast, the cultivated strawberry Fragaria ×  ananassa, an octoploid interspecific hybrid, was developed in the eighteenth century, and the highbush blueberry Vaccinium corymbosum was only domesticated in the twentieth century.Fruit crop species have certain common characteristics: they are long-lived, usually highly heterozygous, and clonally propagated from an elite mother plant. They are also invariably economically minor crops compared with large-scale arable species such as cereals or potatoes, as reflected in the relative research and development resources available. However, fruit is increasingly recognized as an important component of a healthy diet, and consumption of temperate fruit and fruit products, especially juices and dairy derivatives, has risen sharply over the past decade.",3,1);
insert into question (question,answer,expertId,querierId) values ("Climate requirement for Pulses?","Pulse crops are cultivated in Kharif, Rabi and Zaid seasons of the Agricultural year. Rabi crops require mild cold climate during sowing period, during vegetative to pod development cold climate and during maturity / harvesting warm climate. Similarly, Kharif pulse crops require warm climate throughout their life from sowing to harvesting. Summer pulses are habitants of warm climate. Seed is required to pass many stages to produce seed like germination, seedling, vegetative, flowering, fruit setting, pod development and grain maturity / harvesting.",3,4);
insert into question (question,answer,expertId,querierId) values ( "How do I manage pests and diseases on vegetable crops? ","Maintaining a healthy farm or garden is the first step to prevent pest or diseases in vegetables. Some important things to handle so as to keep the plants healthy are:Maintaining the soil fertility which includes loose soil, humus rich content, balanced nutrient levels, the addition of trace elements and most importantly treating the soil organically.Creating a rich biodiversity around the farm or garden can keep the vegetable plants healthy and some measures for biodiversity include creating beneficial habitats (ponds, trees), crop rotations, polyculture, mixed or companion farming.Improving hygiene around the cultivation area such as weed control, removal and disposal of damaged or diseased leaves and plants.Using good quality seeds or stocks as planting material. Also not using too old seeds for planting because they become dormant when stored for more than two years.Always the climate and soil should be thoroughly verified for being suitable for a particular variety of crops before planting.It is always advisable to use resistant varieties to keep the level of disease occurrence to the minimum.Diseases could be avoided by sowing the plants and seeds at the right time.There should be a break in cycle of growing vegetables to prevent re- infection to the new plants.Proper plant and row spacing in the farms could prevent spread of diseases and pests among plants.",3,2);
insert into question (question,answer,expertId,querierId) values ("what are the conditions required for millet farming?","Millets mainly grow well on well-drained loamy soils. Proso Millet does not grow well on sandy soils. For the sprouting and germination of the Millet seeds, warm and temperate climate conditions are essential since they are susceptible to damage by cold weather conditions and frosts.Most Millets have a short growing season and they can be grown well in areas where other crops fail to grow. For example, the Sorghum crop can be cultivated even in drought conditions. Most Millets can do with little moisture since they have effective water utilization abilities.",3,1);


-- view of cart product and sub_category or variety depending on NULL value condition of variety 
drop view if exists vw_cartdetails;

create view vw_cartdetails as select c.cartId,p.subCategoryId,c.userId,c.productId,p.varietyId,if(isnull(p.varietyId),sb.price,v.price) as price,if(isnull(p.varietyId),sb.name,CONCAT(sb.name,'[ ', v.name,' ]')) as name,sb.imageId,c.quantity from cart c left join product p ON c.productId=p.productId left join sub_category sb ON sb.subCategoryId=p.subCategoryId left join variety v ON v.varietyId = p.varietyId;


